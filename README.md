# example-research-project

- readme
    levels
        only notebooks with dependencies
        with python project attached
        how to program against local python package interactively
    gitlab ci
        howto build conda env, get link
        use conda env in wmf data
    unit tests
- gitlabci
    build on demand

## Purpose

This repository is an example repository for research focused projects at Wikimedia, which generally involve
- jupyter notebooks for development, exploration, fast development, and easy sharing of results
- python code that is executed in spark jobs running on the distributed infrastructure;  e.g. the yarn cluster by data engineering
- a python package to be used externally; e.g. by an api endpoint at wmf, a library released to the community, etc

The goal of this repository is to facilitate using shared tooling for projects that aim to do one or more of the use cases above.

## How to use this example project

This is an example project is a plain gitlab project, not a gitlab template that can be forked. The purpose is to provide a starting point for new research projects, and as a reference for existing projects that are migrated to the [research project group on gitlab](https://gitlab.wikimedia.org/repos/research).

1. Create a [new blank project](https://gitlab.wikimedia.org/projects/new?namespace_id=248) in gitlab with an empty readme. Git clone the new repo on your working machine. If there is an existing project you want to migrate, you can [import a project](https://gitlab.wikimedia.org/projects/new?namespace_id=248#import_project) from various locations (e.g. from github), instead of creating a blank project. Consequently, steps 2. and 3. need more care to not accidentally overwrite or break things.
3. Clone this example repository. Copy the files want to use from the example project to your new project. The easiest to do is to copy all files, even if you don't know yet whether you will use that particular component. Note that some files are hidden (e.g. `.gitlab-ci.yml`).
    - python project configuration: `setup.cfg`, `conda-environment.yaml`, `pyproject.toml`
    - tests: `tox.ini`, `tests/`
4. Update the copied file `setup.cfg`
    - `[metadata]` info like name, author, etc
    - `install_requires` to specify dependencies
    If you migrate from a project that uses a setup.py, you can also continue using that as it shouldn't make a difference (theoretically).
5. Commit the changes and push to origin. Go the `CI/DC` section of your project's gitlab page (e.g. [example-research-project](https://gitlab.wikimedia.org/repos/research/example-research-project/-/pipelines)) to verify that gitlab can install/test the project successfully.

## Use cases

### Jupyter notebooks

Juptyer notebooks are a central tool, often they are developed via [JupyterLab](https://wikitech.wikimedia.org/wiki/Analytics/Systems/Jupyter) running on the data engineering infrastructure. Notebooks are stored in the `notebooks` folder in this repository, but this is not a requirement.

#### Notebook only

The [stand_alone.ipynb](./notebooks/stand_alone.ipynb) does not need to be in a gitlab repository containing a python package, it uses to tooling from [research-common](https://gitlab.wikimedia.org/repos/research/research-common.git) directly. Note that it is generally preferable to keep notebooks in a repository.


#### Notebook with gitlab repository

Adding notebooks to a git repository makes them easier to share, discover, and maintain. It is also common that the research project depends on packages that are not available by default in the distributed systems.

The [example_link_graph.ipynb](./notebooks/example_link_graph.ipynb) shows how to use a notebook that is in a repository that also defines a python package (i.e. it has a `setup.cfg` or `setup.py`).

1. Verify that the CI for that gitlab repository works as intended. The `CI/DC` section shows a successful `publish_conda_env` pipeline, if needed you can trigger a run manually via the UI. The `Packages & Registries` section contains a packed conda environment, e.g. [example-research-project-0.0.1.conda.tgz](https://gitlab.wikimedia.org/repos/research/example-research-project/-/packages/125)
2. Clone the repository and open the ipython notebook. E.g. if using Jupyterlab, clone the repository on a stat machine, start a server with an appropriate conda environment, and open the notebook in Jupyterlab.

### Scaffolding

The `example-research-project` repository consists of
- a python package called `example_research`, defined in [setup.cfg](./setup.cfg). The files `conda-environment.yaml`, `pyproject.toml` are part of the python configuration, but don't need to be adapated generally.
- a gitlab Continuous Integration (CI) configuration [.gitlab-ci.yml ](./.gitlab-ci.yml)
- a `tests` directory and testing configuration [tox.ini](./tox.ini). The tests are run for every commit pushed to gitlab.

The CI and testing configuration is minimal and not specific to any repository, and can thus be copied and re-used as is. As a project becomes more complex, this configuration can be adjusted.

#### Python package

When working on a research project using Jupyter notebooks, having a colocated python package enables
- to easily add dependencies to the conda environment used on distributed systems (e.g. in a spark job).
    1. Add the dependency to the `setup.cfg`/`setup.py` file, push commit
    2. Trigger the `publish_conda_env` CI pipeline
    3. Use built conda environment, e.g. using [create_yarn_spark_session](https://gitlab.wikimedia.org/repos/research/research-common/-/blob/main/research_common/spark.py#L19)
- the researcher to gradually move code from notebook cells to the package, to serve use-cases such as:
    - a library to query the output of the project, e.g. to be used by other researchers, or a product team at wmf, etc
    - machine learning data pipelines executed in production as airflow jobs
    - a API service deployed by the [wikimedia deployment pipeline](https://wikitech.wikimedia.org/wiki/Deployment_pipeline)
