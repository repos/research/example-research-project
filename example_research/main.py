import argparse
from research_common.spark import create_yarn_spark_session


def link_graph_main():
    """
    spark2-submit \
        --master yarn \
        ../example_research/main.py \
        --save_table fab.link_graph_simplewiki \
        --wiki_db simplewiki \
        --mediawiki_snapshot 2022-04
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--mediawiki_snapshot',
                        default='2022-04',
                        help='Mediawiki snapshot')
    parser.add_argument('--wiki_db',
                        default='simplewiki',
                        help='wikipedia to extract links for')
    parser.add_argument('--save_table',
                        help='Hive table for storing link delta')

    args = parser.parse_args()

    spark = create_yarn_spark_session(
        app_id='link_graph_as_main',
        gitlab_project='repos/research/example-research-project'
    )

    from example_research.link_graph import extract_link_graph

    (extract_link_graph(spark, args.mediawiki_snapshot, args.wiki_db)
        .write
        .mode("overwrite")
        .saveAsTable(args.save_table)
    )

if __name__ == '__main__':
    link_graph_main()
