from pyspark.sql import functions as F
from pyspark.sql import Row
from pyspark.sql.window import Window
import wikitextparser as wtp

# udf that returns the wikitext size in kilobytes
@F.udf(returnType='int')
def PP_kb(wt):
    return len(wt.encode('utf-8'))//1024

# udf to extract the titles of the linked pages from a revision text
@F.udf(returnType='string')
def PP_extract_links(wt):
    wl = wtp.parse(wt).wikilinks
    wiki_links = { l.title for l in wl }
    wiki_links_string = '#'.join(wiki_links)
    return wiki_links_string


# udf to compare two sets of links
@F.udf(returnType='struct<added:array<string>,removed:array<string>>')
def PP_added_removed_links(curr_links, prev_links):
    prev_links = set(prev_links.split('#'))
    curr_links = set(curr_links.split('#'))
    return Row(
        added=list(curr_links.difference(prev_links)),
        removed=list(prev_links.difference(curr_links))
    )


def extract_link_graph(
    spark,
    mw_snapshot,
    wiki_db):
    """
    Returns a dataframe of wikitext history, but instead of a `revision_text` column with the
    wikitext, it contains a `wiki_links` column with the links that were added and removed in
    that revision.

    :param mw_snapshot:
        spark context to use for pipeline
    :param mw_snapshot:
        The mediawiki snapshot to use (e.g. '2022-03')
    :param wiki_db:
        wiki db to use (e.g. `simplewiki`)
    :return:
        dataframe with column `wiki_links: struct<added:array<string>,removed:array<string>>`
    """

    wikitext_df = (spark
        .read
        .format("avro")
        .load(f"hdfs:///wmf/data/wmf/mediawiki/wikitext/history/snapshot={mw_snapshot}/wiki_db={wiki_db}")
    )

    wiki_links_df = (wikitext_df
        .where(F.col('page_namespace') == 0)
        .where(PP_kb(F.col("revision_text")) < 1024) # smaller than 1mb, todo: looking at >1mb is a good chance at vandalism-
        .withColumn("wiki_links", PP_extract_links(F.col("revision_text")))
        .drop(F.col("revision_text"))
        )

    rev_history = Window.partitionBy("page_id").orderBy("revision_id")
    link_graph_df = (wiki_links_df
        .withColumn('added_removed_links', PP_added_removed_links(
            F.col('wiki_links'),
            F.lag(F.col('wiki_links'), count=1, default="").over(rev_history)
        ))
    )
    return link_graph_df